#include <stdio.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include <ros/ros.h>
#include <image_transport/image_transport.h>

// Message includes
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

// Planar extraction includes
#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>

// Viz includes
#include <boost/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>

// Background Subtraction
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <pcl/common/transforms.h>
#include <Eigen/QR>
#include <pcl/filters/passthrough.h>

using namespace std;
using namespace cv;

typedef pcl::PointCloud<pcl::PointXYZRGB> RGBPointCloud;

// Util functions 

/// Convert the floating point image from the sensor to displayable mono8, stolen from RGBDSLAM
void depthToCV8UC1(const Mat& float_img, Mat& mono8_img) {
	// if mono8_img (the output) is not the right size, initialize it with the right size
	if(mono8_img.rows != float_img.rows || mono8_img.cols != float_img.cols) {
		mono8_img = Mat(float_img.size(), CV_8UC1);
	}
	convertScaleAbs(float_img, mono8_img, 35, 0.0);
}

/// Masks out everything greater than val, 
Mat maskBackground(const Mat& img, const int val) {
	Mat out = img;
	Mat mask = img > val;
	out.setTo(0, mask);
	return out;
}

void dispPoint(
		const boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer, 
		const pcl::PointXYZ& point,
		const string name,
		const uint8_t r, 
		const uint8_t g, 
		const uint8_t b) 
{
	if (!viewer->updateSphere(point, 0.5, r, g, b, name))
		viewer->addSphere(point, 0.5, r, g, b, name);

}

void dispPoint(
		const boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer, 
		const float x,
		const float y,
		const float z,
		const string name,
		const uint8_t r, 
		const uint8_t g, 
		const uint8_t b) 
{
	pcl::PointXYZ point (x, y, z);
	if (!viewer->updateSphere(point, 0.5, r, g, b, name))
		viewer->addSphere(point, 0.5, r, g, b, name);
}


/// Main process containing ROS node, PCL viewer and segmentation
class Segmenter {

	// Two extractors to stop bug
	pcl::ExtractIndices<pcl::PointXYZRGB> extract;
	pcl::ExtractIndices<pcl::PointXYZRGB> extractI;
	pcl::ExtractIndices<pcl::PointXYZRGB> extract2;
	pcl::ExtractIndices<pcl::PointXYZRGB> extractI2;
	// Containers
	pcl::PCLPointCloud2 pcl_pc;
	RGBPointCloud::Ptr ps;

	// PCL Objects
	pcl::SACSegmentation<pcl::PointXYZRGB> seg;
	pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
	pcl::VoxelGrid<pcl::PointXYZRGB> vg;
	pcl::PassThrough<pcl::PointXYZRGB> passThrough;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
	boost::thread viz;
	/// The callback for the subscriber
	void processDepth(const sensor_msgs::ImageConstPtr& msg);
	/// Wrapper for the PCL planar segmentation methods
	pcl::PointIndices::Ptr planeSegmentation(const RGBPointCloud::Ptr& pc, pcl::ModelCoefficients::Ptr& modelcoeffs);
	/// The callback for the point subscriber
	void processPoints(const sensor_msgs::PointCloud2ConstPtr& msg);
	public:
		Segmenter(int argc, char **argv);
		~Segmenter();
		void visualization();
		void extractBackground( 
				const pcl::PointIndicesPtr& inliers,
				const RGBPointCloud::Ptr& cloud, 
				const pcl::ModelCoefficientsPtr& model, 
				RGBPointCloud::Ptr& foregrnd, 
				RGBPointCloud::Ptr& backgrnd);
};

/// The callback for the depth subscriber. Converts and masks out far distances
void Segmenter::processDepth(const sensor_msgs::ImageConstPtr& msg) {
	cv_bridge::CvImagePtr ptr;
	try {
		ptr = cv_bridge::toCvCopy(msg);
	} catch (cv::Exception& e) {
		//ROS_ERROR("Could not convert from %s to bgr8.", msg->encoding.c_str());
	}
	//Convert the CvImagePtr to cv Mats
	Mat depth_float_img = ptr->image;
	Mat depth_mono8_img;
	depthToCV8UC1(depth_float_img, depth_mono8_img);
	//Display
	imshow("Disparity", maskBackground(depth_mono8_img, 120));
	waitKey(30);
}
/// Wrapper for the PCL planar segmentation methods
pcl::PointIndices::Ptr Segmenter::planeSegmentation(const RGBPointCloud::Ptr& pc, pcl::ModelCoefficients::Ptr& outcoeffs) {
	pcl::ModelCoefficients::Ptr coeffs (new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
	seg.setInputCloud(pc);
	seg.segment(*inliers, *coeffs);
	if (inliers->indices.size() == 0) {
		ROS_ERROR("Could not estimate a planar model for the current point cloud.");
		//return NULL; // Changed because was using c11, now using older version
	}
	// Return both coeffs and inliers
	outcoeffs = coeffs;
	return inliers;
}

void Segmenter::extractBackground(
		const pcl::PointIndicesPtr& inliers, 
		const RGBPointCloud::Ptr& cloud, 
		const pcl::ModelCoefficientsPtr& model,
		RGBPointCloud::Ptr& foregrnd,
		RGBPointCloud::Ptr& backgrnd) {
	/**
	 * Overview: 
	 * 1. Extract points from ground plane 
	 * 2. Get furthest point along the z axis 
	 * 3. Get average offset from origin 
	 * 4. Create translation matrix based on offset 
	 * 5. Do QR decomposition of rest of points 
	 * 6. Create transform matrix based on rotation 
	 * 7. Realign points with z axis
	 * 8. Apply transformations 
	 * 9. PassThrough filter back plane based on furthest point from REST of points 
	 */
	
	// 1. Extract points
	RGBPointCloud::Ptr rest (new RGBPointCloud); // variable to hold ground plane
	RGBPointCloud::Ptr floor_cloud (new RGBPointCloud); // variable to hold ground plane
	this->extract.setInputCloud(cloud);
	this->extract.setIndices(inliers);
	this->extract.filter(*floor_cloud);
	// extract the points excluding the floor ( for subtracting background )
	this->extractI.setInputCloud(cloud);
	this->extractI.setIndices(inliers);
	this->extractI.filter(*rest); 

	// 2. Get furthest point along the z axis
	float max = floor_cloud->points.at(0).z, min = floor_cloud->points.at(0).z;
	size_t size = floor_cloud->points.size();

	float x_array[size];
	float y_array[size];
	float z_array[size];

	int max_i = 0, min_i= 0;
	for(size_t i=0; i<size; ++i) {
		if(max < floor_cloud->points[i].z) {
			max = floor_cloud->points[i].z;
			max_i = i;
		}
		if(min > floor_cloud->points[i].z) {
			min = floor_cloud->points[i].z;
			min_i = i;
		}
		x_array[i] = floor_cloud->points[i].x;
		y_array[i] = floor_cloud->points[i].y;
		z_array[i] = floor_cloud->points[i].z;
	}
	// For getting the median
	sort(x_array, x_array+size);
	sort(y_array, y_array+size);
	sort(z_array, z_array+size);

	float x_bar = x_array[int(size/2)];
	float y_bar = y_array[int(size/2)];
	float z_bar = z_array[int(size/2)];
	// 3. Get average offset from origin
	float x_mean = accumulate(x_array, x_array+size, float(0)) / (size+1);
	float y_mean = accumulate(y_array, y_array+size, float(0)) / (size+1);
	float z_mean = accumulate(z_array, z_array+size, float(0)) / (size+1);

	// 4. Create translation matrix based on offset
	Eigen::Matrix4f transform;
	transform << 1, 0, 0, -x_bar,
				 0, 1, 0, -y_bar,
				 0, 0, 1, -z_bar,
				 0, 0, 0, 1;

	

	float a = model->values[0], b = model->values[1], c = model->values[2];//, d = model->values[3];

	// 5. Do QR decomposition of rest of points
	Eigen::Matrix3f A;
	A << a, 0, 0,
		 b, 1, 0,
		 c, 0, 1;
	Eigen::HouseholderQR<Eigen::Matrix3f> qr(A);
	Eigen::Matrix3f swap;
	swap << 0, 1, 0,
			0, 0, 1,
			1, 0, 0;
	Eigen::Matrix3f Q = (qr.householderQ() * swap).inverse();
	//cout << "Q: " << "A: " << a << " B: " << b << " C: " << c << " Det: " << Q.determinant() << "\n\n";;
	//cout << Q << "\n\n";
	// 6. Create transform matrix based on rotation
	Eigen::Matrix4f rotation;
	rotation << 0, 0, 0, 0,
				0, 0, 0, 0,
				0, 0, 0, 0,
				0, 0, 0, 1;
	rotation.block<3, 3>(0,0) = Q;
	// 7. Realign points with z axis
	Eigen::Vector3f ea = Q.eulerAngles(0, 1, 2);
	/*Eigen::Matrix4f composite;
	composite << 0, 0, 0, 0,
				 0, 0, 0, 0,
				 0, 0, 0, 0,
				 0, 0, 0, 1;
	Eigen::Matrix3f n;
	n = Eigen::AngleAxisf(ea[0], Eigen::Vector3f::UnitX()) *
		Eigen::AngleAxisf(ea[1], Eigen::Vector3f::UnitY()) *
		Eigen::AngleAxisf(ea[2], Eigen::Vector3f::UnitZ());
	composite.block<3,3>(0,0) = n;*/

	Eigen::Matrix4f correction;
	correction << 1, 0, 0, 0,
				  0, 1, 0, 0,
				  0, 0, 1, 0,
				  0, 0, 0, 1;
	correction.block<3,3>(0,0) = Eigen::Matrix3f(Eigen::AngleAxisf(-ea[1], Eigen::Vector3f::UnitY()));

	// 8. Apply transformations
	RGBPointCloud::Ptr rot_floor (new RGBPointCloud);
	pcl::transformPointCloud(*floor_cloud, *rot_floor, correction*rotation*transform);
	RGBPointCloud::Ptr rot_cloud (new RGBPointCloud);
	pcl::transformPointCloud(*rest, *rot_cloud, correction*rotation*transform);
	// 8. PassThrough filter back plane based on furthest point from REST of points
	RGBPointCloud::Ptr back_cloud (new RGBPointCloud);
	RGBPointCloud::Ptr foregrnd_cloud (new RGBPointCloud);
	pcl::PointIndices::Ptr foregrnd_indices (new pcl::PointIndices);
	// Use the max point for filtering
	//float dist = sqrt(pow(rot_floor->points[max_i].z, 2) + pow(rot_floor->points[max_i].x, 2));
	// Use the 75% point
	for (int i=0; i<size; i++) {
		z_array[i] = rot_floor->points[i].z;
	}
	sort(z_array, z_array+size);
	int n75 = int(floor(.95*size));
	float dist = sqrt(pow(z_array[n75], 2) + pow(z_array[n75], 2));

	this->passThrough.setInputCloud(rot_cloud);
	this->passThrough.setFilterLimits(dist, dist+10);
	this->passThrough.filter(foregrnd_indices->indices);
	//pcl::PointIndicesPtr indices_ptr (foregrnd_indices);
	this->extractI2.setInputCloud(rot_cloud);
	this->extractI2.setIndices(foregrnd_indices);
	this->extractI2.filter(*foregrnd_cloud); 
	this->extract2.setInputCloud(rot_cloud);
	this->extract2.setIndices(foregrnd_indices);
	this->extract2.filter(*back_cloud); 

	*back_cloud += *rot_floor;

	// return
	foregrnd = foregrnd_cloud;
	backgrnd = back_cloud;
}

/// The callback for the point subscriber
void Segmenter::processPoints(const sensor_msgs::PointCloud2ConstPtr& msg) {
	// Convert points to PCL PointCloud<PointXYZRGB>
	pcl_conversions::toPCL(*msg, this->pcl_pc);
	pcl::fromPCLPointCloud2(this->pcl_pc, *ps);
	
	RGBPointCloud::Ptr grid (new RGBPointCloud);
	vg.setInputCloud(ps);
	vg.filter(*grid);

	RGBPointCloud::Ptr filtered (new RGBPointCloud);
	sor.setInputCloud(grid);
	sor.filter(*filtered);
	//if (!viewer->updatePointCloud(filtered, "cloud"))
		//viewer->addPointCloud(filtered, "cloud");
	// Perform segmentation
	pcl::ModelCoefficients::Ptr model;
	pcl::PointIndices::Ptr inliers = planeSegmentation(filtered, model);
	cout << inliers->indices.size() << endl;
	if (inliers->indices.size() > 0) {
		// Background foreground seperation
		RGBPointCloud::Ptr foregrnd, backgrnd;
		this->extractBackground(inliers, filtered, model, foregrnd, backgrnd);

		// Update viewer
		//if (!viewer->updatePointCloud(foregrnd, "cloud"))
			//viewer->addPointCloud(foregrnd, "cloud");
		if (!viewer->updatePointCloud(backgrnd, "r_cloud"))
			viewer->addPointCloud(backgrnd, "r_cloud");
		//viewer->spinOnce();
	}
}

/// Starts ROS nodes, intializes helper objects and creates the windows
Segmenter::Segmenter(int argc, char **argv) :
	ps(new RGBPointCloud),
	viewer (new pcl::visualization::PCLVisualizer ("Point Cloud Viewer")),
	// Start visualization thread to keep the window alive
	viz(boost::bind(&Segmenter::visualization, this))
{
	cv::namedWindow("Disparity");
	cv::startWindowThread();

	// Start ROS subs
	ros::init(argc, argv, "seg_test");
	ros::NodeHandle n;
	image_transport::ImageTransport it(n);
	ros::Subscriber point_sub = n.subscribe("/camera/depth_registered/points", 1, &Segmenter::processPoints, this);
	image_transport::Subscriber disp_sub = it.subscribe("/camera/depth/image", 1, &Segmenter::processDepth, this);
	
	// Set up PCL extractor
	this->extract.setNegative(false);
	this->extractI.setNegative(true);
	this->extract2.setNegative(false);
	this->extractI2.setNegative(true);

	this->sor.setMeanK(50);
	this->sor.setStddevMulThresh(1.0);

	this->vg.setLeafSize(0.02f, 0.02f, 0.02f);

	// Set up PCL segmenter
	seg.setOptimizeCoefficients (true);
	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	//seg.setMaxIterations(1000);
	seg.setDistanceThreshold(0.06);

	// Set up PCL PassThrough filter
	passThrough.setFilterFieldName("z");

	// Create sample cloud
	RGBPointCloud::Ptr sampleCloud(new RGBPointCloud);
	sampleCloud->width = 15; sampleCloud->height = 1;
	sampleCloud->points.resize (sampleCloud->width * sampleCloud->height);
	for (size_t i = 0; i < sampleCloud->points.size(); ++i) {
		sampleCloud->points[i].x = 1024 * rand() / (RAND_MAX + 1.0f);
		sampleCloud->points[i].y = 1024 * rand() / (RAND_MAX + 1.0f);
		sampleCloud->points[i].z = 1.0;
	}

	// Set up viewer
	viewer->setBackgroundColor(0,0,0);
	viewer->addPointCloud<pcl::PointXYZRGB>(sampleCloud, "cloud");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud");
	viewer->addCoordinateSystem(1.0);
	viewer->initCameraParameters();

	// Spin up ROS
	ros::spin();
}

Segmenter::~Segmenter() {
	cv::destroyWindow("Disparity");
}

// Thread function to keep PCL viewer awake
void Segmenter::visualization() {
	boost::this_thread::interruption_enabled();
	while (!this->viewer->wasStopped()) {
		this->viewer->spinOnce();
		boost::this_thread::sleep(boost::posix_time::microseconds(10));
	}
}

int main(int argc, char **argv) {
	Segmenter seg(argc, argv);
	return 0;
}
