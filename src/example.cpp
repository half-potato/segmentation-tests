#include <stdio.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include <pcl/pcl_base.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl_ros/point_cloud.h>

using namespace std;

pcl::visualization::CloudViewer GLOBAL_VIEWER("Simple Butt Viewer");
pcl::PointCloud<pcl::PointXYZRGB>::Ptr SAMPLE_CLOUD(new pcl::PointCloud<pcl::PointXYZRGB>);

void visualization() {
	while (!GLOBAL_VIEWER.wasStopped()) {
	}
}

int main(int argc, char **argv) {
	SAMPLE_CLOUD->width = 15; SAMPLE_CLOUD->height = 1;
	SAMPLE_CLOUD->points.resize (SAMPLE_CLOUD->width * SAMPLE_CLOUD->height);
	for (size_t i = 0; i < SAMPLE_CLOUD->points.size(); ++i) {
		SAMPLE_CLOUD->points[i].x = 1024 * rand() / (RAND_MAX + 1.0f);
		SAMPLE_CLOUD->points[i].y = 1024 * rand() / (RAND_MAX + 1.0f);
		SAMPLE_CLOUD->points[i].z = 1.0;
	}
	GLOBAL_VIEWER.showCloud(SAMPLE_CLOUD);
	boost::thread viz(visualization);
	viz.join();
	return 0;
}

