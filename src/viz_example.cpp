#include <stdio.h>
#include <iostream>
#include <math.h>
#include <vector>

#include <boost/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/io/pcd_io.h>

using namespace std;

boost::shared_ptr<pcl::visualization::PCLVisualizer> GLOBAL_VIEWER;
pcl::PointCloud<pcl::PointXYZ>::Ptr SAMPLE_CLOUD(new pcl::PointCloud<pcl::PointXYZ>);

boost::shared_ptr<pcl::visualization::PCLVisualizer> createViewer(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud) {
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("Hard Butt Viewer"));
	viewer->setBackgroundColor(0,0,0);
	viewer->addPointCloud<pcl::PointXYZ>(cloud, "sample");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample");
	viewer->addCoordinateSystem(1.0);
	viewer->initCameraParameters();
	return viewer;
}

void visualization() {
	while (!GLOBAL_VIEWER->wasStopped()) {
		GLOBAL_VIEWER->spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}
}

int main(int argc, char **argv) {
	SAMPLE_CLOUD->width = 15; SAMPLE_CLOUD->height = 1;
	SAMPLE_CLOUD->points.resize (SAMPLE_CLOUD->width * SAMPLE_CLOUD->height);
	for (size_t i = 0; i < SAMPLE_CLOUD->points.size(); ++i) {
		SAMPLE_CLOUD->points[i].x = 1024 * rand() / (RAND_MAX + 1.0f);
		SAMPLE_CLOUD->points[i].y = 1024 * rand() / (RAND_MAX + 1.0f);
		SAMPLE_CLOUD->points[i].z = 1.0;
	}
	GLOBAL_VIEWER = createViewer(SAMPLE_CLOUD);
	boost::thread viz(visualization);
	viz.join();
	return 0;
}

